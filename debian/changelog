librecast (0.5.1-4) unstable; urgency=medium

  [ Pino Toscano ]
  * Move development man pages to liblibrecast-dev: they document the API,
    so they are not of much use in the library package
    - move the liblibrecast0.4 breaks/replaces from liblibrecast0.5 to
      liblibrecast-dev
    - add breaks/replaces in liblibrecast-dev against liblibrecast0.5

 -- Vagrant Cascadian <vagrant@debian.org>  Wed, 10 Aug 2022 15:36:41 -0700

librecast (0.5.1-3) unstable; urgency=medium

  * debian/control: Set default Section to "libs".
    (Closes: #1016848)

 -- Vagrant Cascadian <vagrant@debian.org>  Tue, 09 Aug 2022 17:09:40 -0700

librecast (0.5.1-2) unstable; urgency=medium

  * debian/patches: Make test 0012 time more robust.
  * debian/rules: Output the results of running the test suite.
  * debian/control: Add Breaks and Replaces on liblibrecast0.4. Thanks to
    Axel Beckert.  (Closes: #1016565)
  * debian/patches: Directly symlink the .so ABI version file.

 -- Vagrant Cascadian <vagrant@debian.org>  Thu, 04 Aug 2022 12:54:16 -0700

librecast (0.5.1-1) experimental; urgency=medium

  * debian/patches: Remove patches applied upstream.
  * debian/control: Add Build-Depends on liblcrq-dev.
  * debian/control: Update short descriptions.

 -- Vagrant Cascadian <vagrant@debian.org>  Wed, 20 Jul 2022 13:07:13 -0700

librecast (0.5.0-1) experimental; urgency=medium

  * debian/control: Update Homepage.
  * debian/copyright, debian/watch: Update upstream URLs.
  * debian/copyright: Update for 0.5.0.
  * debian/patches: Add patches from upstream to fix build failures.
  * Update packaging for new ABI.
  * debian/control: Update Standards-Version to 4.6.1.
  * debian/patches: Drop modifications to CHANGELOG.md.

 -- Vagrant Cascadian <vagrant@debian.org>  Thu, 14 Jul 2022 13:52:28 -0700

librecast (0.4.5-1) experimental; urgency=medium

  * New upstream release.
  * debian/watch: Switch to monitoring tags.
  * debian/control: Update Standards-Version to 4.6.0, no changes.

 -- Vagrant Cascadian <vagrant@debian.org>  Wed, 06 Apr 2022 11:59:13 -0700

librecast (0.4.4-1) experimental; urgency=medium

  * New upstream release.
  * debian/copyright: Update with new licensing.
  * debian/patches: Remove patch, applied upstream.

 -- Vagrant Cascadian <vagrant@debian.org>  Fri, 11 Jun 2021 10:59:19 -0700

librecast (0.4.3-1) experimental; urgency=medium

  * New upstream release.
  * debian/control: liblibrecast-dev: Add depends on
    liblibrecast0.4. Thanks to Andreas Beckmann. (Closes: #985694)
  * Use libsodium on all architectures.
  * debian/patches: Remove patch to use blake3 in top-level directory.
  * debian/copyright: Remove blake3, no longer shipped.
  * debian/patches: Fix when building without blake3.

 -- Vagrant Cascadian <vagrant@debian.org>  Fri, 02 Apr 2021 11:57:09 -0700

librecast (0.4.2-2) experimental; urgency=medium

  * debian/control: Add Vcs-* headers.
  * Switch back to using libsodium on non-x86 architectures.
  * debian/control: Use multicast instead of multi-cast in Description.

 -- Vagrant Cascadian <vagrant@debian.org>  Mon, 08 Mar 2021 15:59:56 -0800

librecast (0.4.2-1) experimental; urgency=medium

  * Move include files into liblibrecast-dev.
  * debian/patches: Remove all patches, applied upstream.
  * debian/rules: dh_auto_test: Remove CFLAGS workaround, fixed upstream.
  * debian/control: Drop Build-Depends on libsodium-dev.
  * debian/copyright: Update for blake3.
  * debian/patches: Use blake3 in top level directory.

 -- Vagrant Cascadian <vagrant@debian.org>  Mon, 08 Mar 2021 13:49:56 -0800

librecast (0.4.1-1) experimental; urgency=low

  * Initial release.

 -- Vagrant Cascadian <vagrant@debian.org>  Fri, 05 Mar 2021 17:44:47 -0800
