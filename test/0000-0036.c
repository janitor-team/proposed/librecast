/* SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only */
/* Copyright (c) 2017-2022 Brett Sheffield <bacs@librecast.net> */

#include "test.h"
#include <librecast/crypto.h>
#include <librecast/net.h>
#include <pthread.h>
#include <semaphore.h>

#define WAITS 1

#ifdef HAVE_LIBSODIUM
static char channel_name[] = "0000-0036";
static sem_t sem;
static ssize_t bytes = -1;
static unsigned char key[crypto_secretbox_KEYBYTES];

static void *listen_thread_crypt(void *arg)
{
	lc_ctx_t * lctx;
	lc_socket_t * sock;
	lc_channel_t * chan;

	lctx = lc_ctx_new();
	sock = lc_socket_new(lctx);
	chan = lc_channel_new(lctx, channel_name);
	lc_channel_set_sym_key(chan, key, crypto_secretbox_KEYBYTES);
	lc_channel_coding_set(chan, LC_CODE_SYMM);

	lc_channel_bind(sock, chan);
	lc_channel_join(chan);

	sem_post(&sem); /* tell send thread we're ready */
	bytes = lc_socket_recv(sock, (char *)arg, BUFSIZ, 0);
	sem_post(&sem); /* tell send thread we're done */

	lc_ctx_free(lctx);
	return arg;
}

static void *listen_thread(void *arg)
{
	lc_ctx_t * lctx;
	lc_socket_t * sock;
	lc_channel_t * chan;

	lctx = lc_ctx_new();
	sock = lc_socket_new(lctx);
	chan = lc_channel_new(lctx, channel_name);

	lc_channel_bind(sock, chan);
	lc_channel_join(chan);

	sem_post(&sem); /* tell send thread we're ready */
	bytes = lc_socket_recv(sock, (char *)arg, BUFSIZ, 0);
	sem_post(&sem); /* tell send thread we're done */

	lc_ctx_free(lctx);
	return arg;
}
#endif

int main(void)
{
#ifndef HAVE_LIBSODIUM
	return test_skip("lc_channel_send() / lc_socket_recv() - symmetric encryption");
#else
	lc_ctx_t * lctx;
	lc_socket_t * sock;
	lc_channel_t * chan;
	pthread_t thread;
	struct timespec ts;
	char buf[] = "liberté";
	char recvbuf[BUFSIZ] = "";
	size_t len = sizeof buf;
	ssize_t elen = len + crypto_secretbox_MACBYTES + crypto_secretbox_NONCEBYTES;

	test_name("lc_channel_send() / lc_socket_recv() - symmetric encryption");

	sem_init(&sem, 0, 0);

	pthread_create(&thread, NULL, &listen_thread, &recvbuf);

	sem_wait(&sem); /* recv thread is ready */

	lctx = lc_ctx_new();
	sock = lc_socket_new(lctx);
	chan = lc_channel_new(lctx, channel_name);

	/* generate and set symmetric key on sender */
	crypto_secretbox_keygen(key);
	lc_channel_set_sym_key(chan, key, crypto_secretbox_KEYBYTES);
	lc_channel_coding_set(chan, LC_CODE_SYMM);

	lc_socket_loop(sock, 1);
	lc_channel_bind(sock, chan);
	lc_channel_send(chan, buf, len, 0);

	test_assert(!clock_gettime(CLOCK_REALTIME, &ts), "clock_gettime()");
	ts.tv_sec += WAITS;
	test_assert(!sem_timedwait(&sem, &ts), "timeout");
	sem_timedwait(&sem, &ts);
	sem_destroy(&sem);

	pthread_cancel(thread);
	pthread_join(thread, NULL);

	test_assert(bytes == elen, "received %zi bytes, expected %zu", bytes, len);

	/* we can't really prove the data is encrypted, but we can at least
	 * check it isn't the same as what we sent */
	test_assert(memcmp(buf, recvbuf, sizeof buf) != 0, "data doesn't match with encryption");

	/* set receiver channel key and re-test */
	bytes = -1;
	pthread_create(&thread, NULL, &listen_thread_crypt, &recvbuf);
	sem_wait(&sem); /* recv thread is ready */
	lc_channel_send(chan, buf, len, 0); /* send again */

	test_assert(!clock_gettime(CLOCK_REALTIME, &ts), "clock_gettime()");
	ts.tv_sec += WAITS;
	test_assert(!sem_timedwait(&sem, &ts), "timeout");
	sem_timedwait(&sem, &ts);
	sem_destroy(&sem);
	pthread_cancel(thread);
	pthread_join(thread, NULL);

	/* test we can decrypt message */
	test_assert(bytes == (ssize_t)len, "received %zi bytes, expected %zu", bytes, len);
	test_expect(buf, recvbuf);

	lc_ctx_free(lctx);

	return fails;
#endif
}
