# SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only
# Copyright (c) 2017-2022 Brett Sheffield <bacs@librecast.net>

SHELL := bash
VERSION := @PACKAGE_VERSION@
ABIVERS := @PACKAGE_ABIVERS@
CFLAGS += -fno-builtin-malloc -fno-builtin-calloc -I. -I../src -I../include -I../libs/blake3/c
LDFLAGS += -L../src -L../libs/blake3/c -Wl,-rpath,../src
LDLIBS += -llibrecast -lrt -pthread @LIBDL@ @LIBBLAKE3@ @LIBSODIUM@
NOTOBJS := ../src/$(PROGRAM).o
OBJS := test.o misc.o falloc.o

ifeq ($(OSNAME),FreeBSD)
LDCONFIG := ln -sf ../src/liblibrecast.so ../src/liblibrecast.so.$(ABIVERS)
endif
ifeq ($(OSNAME),NetBSD)
LDCONFIG := ln -sf ../src/liblibrecast.so ../src/liblibrecast.so.$(ABIVERS)
endif
ifeq ($(OSNAME),Linux)
LDCONFIG := ldconfig -n ../src
endif

export LD_LIBRARY_PATH := "../src ../libs/blake3/c ${LD_LIBRARY_PATH}"

BOLD := "\\e[0m\\e[2m"
RESET := "\\e[0m"
PASS = "\\e[0m\\e[32mOK\\e[0m" # end bold, green text
FAIL = "\\e[0m\\e[31mFAIL\\e[0m" # end bold, red text
LEAK = "\\e[0m\\e[31mFAIL \| LEAK\\e[0m\\n\\tSee $@.valgrind"
LASTLOG := lastlog.log
LOGFILE := $(shell mktemp "lsdb-test-XXXXXXXX")
TIMESTAMP = $(shell date +"%Y-%m-%d %H:%M:%S")
VALGRIND = valgrind --leak-check=full --max-stackframe=20480160 --error-exitcode=2 --errors-for-leak-kinds=all --track-origins=yes --log-file=$@.valgrind

.PHONY: test clean realclean build result check

check: MEMCHECK = $(VALGRIND)
check: FAIL = $(LEAK)
check: test

test: clean build $(shell echo ????-????.c | sed 's/\.c/\.test/g') result

build:
	$(MAKE) -C ../src
	$(LDCONFIG)
	@echo -e "$(TIMESTAMP) - starting tests" >> $(LOGFILE)
	@echo -e "\nRunning tests"

# FIXME dial down optimisation for this test or it breaks under valgrind
#0000-0026.test: CFLAGS += -O

%.test: %.c build $(OBJS)
	@$(CC) $(CFLAGS) $(CPPFLAGS) $(LDFLAGS) -o $@ $< $(OBJS) $(LDLIBS)
	@echo -ne "\e[2m" $* " "
	@echo -ne "\n== $@" >> $(LOGFILE)
	@LD_LIBRARY_PATH=$(LIBRARY_PATH) $(MEMCHECK) ./$@ 2>> $(LOGFILE) && echo -e " $(PASS)" || echo -e " $(FAIL)"
	@ln -sf $(LOGFILE) $(LASTLOG)
	@$(eval tests_run=$(shell echo $$(($(tests_run)+1))))

%.check: MEMCHECK = $(VALGRIND)
%.check: FAIL = $(LEAK)
%.check: %.test
	@echo "check completed"
	@echo -e "    logfile:   " $(BOLD) $(LOGFILE) / $(LASTLOG) $(RESET)
	@echo -e "    valgrind:  " $(BOLD) "$^.valgrind" $(RESET)

%.debug: MEMCHECK = gdb
%.debug: %.test
	@echo "exiting debugger"

falloc.o: falloc.h

test.o: test.h

result:
	@echo -e "\n$(TIMESTAMP) - tests done" >> $(LOGFILE)
	@echo -e "$(tests_run) tests run\nlogfile: $(LOGFILE)\n"

clean:
	rm -f *.test *.o
	@rm -f $(LOGFILE) $(LASTLOG)

realclean: clean
	rm -f lsdb-test-???????? ????-????.test.valgrind
	rm -rf ????-????.tmp.*

